TestingAirlinesWebsite

# FAQ
------------------

База данных - PostgreSQL 10.4

SQL запросы находятся в корне проекта /scriptsSQL

Java 8

Скрипт для отчета allure в корне проекта /allureBat, запустить .bat файл

Все данные вводятся через файл конфига

------------------

Для работые нужно создать базу данных:

```SQL
CREATE DATABASE aviasalesDB;
```

Далее создать таблицы базы данных:

```SQL
BEGIN;

CREATE TABLE airport (
	name varchar(50) PRIMARY KEY,
	country  varchar(50),
	city varchar(50)
);

CREATE TABLE airlines (
	name varchar(50) PRIMARY KEY,
	country varchar(50),
	airport varchar(50) references airport(name)
);

CREATE TABLE cheapTickets (
	id SERIAL PRIMARY KEY,
	airlines varchar(50) references airlines(name),
	departureAirport varchar(50) references airport(name),
	arrivalAirport varchar(50) references airport(name),
	departureDateAndTime varchar(50),
	arrivalDateAndTime varchar(50),
	priceTicket varchar(50),
	travelTime varchar(50)
);

COMMIT;
```

Далее заполнить таблицу тестовыми данными:

```SQL
BEGIN;

INSERT INTO airport VALUES
	('Шереметьево', 'Россия', 'Москва'),
	('Мюнхен', 'Германия', 'Мюнхен'),
	('Шарль-де-Голль','Франция','Париж'),
	('Шоуду','Китай','Пекин'),
	('Международный аэропорт им. Джона Кеннед','США','Нью-Йорк');

INSERT INTO airlines VALUES
	('Аэрофлот','Россия','Шереметьево'),
	('American Airlines','США','Международный аэропорт им. Джона Кеннед'),
	('Air France','Франция','Шарль-де-Голль'),
	('Air China','Китай','Шоуду'),
	('Air Berlin','Германия','Мюнхен');

INSERT INTO cheaptickets(airlines, departureAirport, arrivalAirport, departureDateAndTime, arrivalDateAndTime, priceTicket, travelTime) VALUES
	('Аэрофлот','Шереметьево','Мюнхен','2017-01-08 04:05:06','2017-01-08 07:05:00','5005.143','03:00:00'),
	('Air France','Шарль-де-Голль','Шереметьево','2017-02-08 01:00:00','2017-02-08 07:00:00','5005.143','06:00:00'),
	('Air China','Шоуду','Мюнхен','2017-05-08 05:00:00','2017-05-08 09:00:00','5005.143','04:00:00'),
	('Air Berlin','Мюнхен','Международный аэропорт им. Джона Кеннед','2017-09-08 06:00:00','2017-11-08 10:00:00','5005.143','04:00:00');

COMMIT;
```
