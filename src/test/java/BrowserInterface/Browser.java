package BrowserInterface;

import org.openqa.selenium.WebDriver;

public abstract class Browser {
    public abstract WebDriver getWebDriver();
}
