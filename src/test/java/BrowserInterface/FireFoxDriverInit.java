package BrowserInterface;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.GeckoDriverService;

import java.io.File;

/**
 * @author Баскаков Алексей
 * Инициализация firefoxdriver
 */
public class FireFoxDriverInit extends Browser {
    private final String URI_DRIVER = "driver/geckodriver.exe";

    /**
     * @return драйвер мозиллы
     */
    @Override
    public WebDriver getWebDriver() {
        GeckoDriverService cds = new GeckoDriverService
                .Builder()
                .usingAnyFreePort()
                .usingFirefoxBinary(
                        new FirefoxBinary(
                                new File("C://Program Files (x86)/Mozilla Firefox/firefox.exe")))
                .usingDriverExecutable(new File(URI_DRIVER))
                .build();

        return new FirefoxDriver(cds);
    }
}
