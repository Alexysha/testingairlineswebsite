package BrowserInterface;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerDriverService;

import java.io.File;

/**
 * @author Баскаков Алексей
 * Инициализация IEDriver
 */
public class IEDriverInit extends Browser {
    private final String URI_DRIVER = "driver/IEDriverServer.exe";

    /**
     * @return драйвер IE
     */
    @Override
    public WebDriver getWebDriver() {
        InternetExplorerDriverService cds = new InternetExplorerDriverService
                .Builder()
                .usingAnyFreePort()
                .usingDriverExecutable(new File(URI_DRIVER))
                .build();

        return new InternetExplorerDriver(cds);
    }
}
