package BrowserInterface;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;

import java.io.File;

/**
 * @author Баскаков Алексей
 * Инициализация driverchrome
 */
public class ChromeDriverInit extends Browser {
    private final String URI_DRIVER = "driver/chromedriver.exe";

    /**
     * @return драйвер хрома
     */
    @Override
    public WebDriver getWebDriver() {
        ChromeDriverService cds = new ChromeDriverService
                .Builder()
                .usingAnyFreePort()
                .usingDriverExecutable(new File(URI_DRIVER))
                .build();

        return new ChromeDriver(cds);
    }
}
