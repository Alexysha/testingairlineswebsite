package modelORM;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * @author Баскаков Алексей
 * ORM модель таблицы airlines
 */
@DatabaseTable(tableName = "airlines")
public class Airlines {

    @DatabaseField(id = true)
    private String name;

    private String country;

    @DatabaseField(foreign = true)
    private String airport;

    public Airlines() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAirport() {
        return airport;
    }

    public void setAirport(String airport) {
        this.airport = airport;
    }
}
