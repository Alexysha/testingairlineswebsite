package modelORM;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * @author Баскаков Алексей
 * ORM модель таблицы cheaptickets
 */
@DatabaseTable(tableName = "cheaptickets")
public class CheapTickets {

    @DatabaseField()
    private String airlines;

    @DatabaseField(columnName = "departureairport")
    private String departureAirport;

    @DatabaseField(columnName = "arrivalairport")
    private String arrivalAirport;

    @DatabaseField(columnName = "departuredateandtime")
    private String departureDateAndTime;

    @DatabaseField(columnName = "arrivaldateandtime")
    private String arrivalDateAndTime;

    @DatabaseField(columnName = "priceticket")
    private String priceTicket;

    @DatabaseField(columnName = "traveltime")
    private String travelTime;


    public String getAirlines() {
        return airlines;
    }

    public void setAirlines(String airlines) {
        this.airlines = airlines;
    }

    public String getDepartureAirport() {
        return departureAirport;
    }

    public void setDepartureAirport(String departureAirport) {
        this.departureAirport = departureAirport;
    }

    public String getArrivalAirport() {
        return arrivalAirport;
    }

    public void setArrivalAirport(String arrivalAirport) {
        this.arrivalAirport = arrivalAirport;
    }

    public String getDepartureDateAndTime() {
        return departureDateAndTime;
    }

    public void setDepartureDateAndTime(String departureDateAndTime) { this.departureDateAndTime = departureDateAndTime; }

    public String getArrivalDateAndTime() {
        return arrivalDateAndTime;
    }

    public void setArrivalDateAndTime(String arrivalDateAndTime) {
        this.arrivalDateAndTime = arrivalDateAndTime;
    }

    public String getPriceTicket() {
        return priceTicket;
    }

    public void setPriceTicket(String priceTicket) {
        this.priceTicket = priceTicket;
    }

    public String getTravelTime() {
        return travelTime;
    }

    public void setTravelTime(String travelTime) {
        this.travelTime = travelTime;
    }
}
