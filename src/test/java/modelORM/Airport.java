package modelORM;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * @author Баскаков Алексей
 * ORM модель таблицы airport
 */
@DatabaseTable(tableName = "airport")
public class Airport {

    @DatabaseField(id = true)
    private String name;
    private String country;
    private String city;


    public Airport() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
