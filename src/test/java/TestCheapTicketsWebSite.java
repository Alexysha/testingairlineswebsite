import BrowserInterface.Browser;
import BrowserInterface.ChromeDriverInit;
import BrowserInterface.FireFoxDriverInit;
import BrowserInterface.IEDriverInit;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import io.qameta.allure.Feature;
import io.qameta.allure.Step;
import io.qameta.allure.Story;
import modelORM.CheapTickets;
import modelPOM.CheapTicketsPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import static java.util.logging.Logger.getLogger;
import static modelPOM.MyWebElementAction.*;

@Feature("Проект по поиску авиабилетов")
public class TestCheapTicketsWebSite {
    private String jdbcUrl;
    private String login;
    private String password;
    private String parseUri;
    private Browser browser;
    private WebDriver driver;
    private ConnectionSource connection;
    private HashMap<String, Browser> hashBrowser = new HashMap<>();

    private final Object[] PAGES = {
            new CheapTicketsPage()
    };

    @BeforeClass
    public void setUp() throws IOException, SQLException {
        initHashMapBrowser(); // заполнение коллекции
        loadProperties(); // чтение конфига

        driver = browser.getWebDriver(); // определение нужного драйвера
        driver.get(parseUri); // Загрузка страницы
        setDriver(driver); // передача драйвера в класс MyWebElementAction
        setPageFactory(PAGES); // заполнение фабрики доступными object page

        connection = new JdbcConnectionSource(jdbcUrl, login, password); // JDBC подключение
    }

    @Test
    @Story("Парсинг html разметки сайта")
    public void checkCheapTickets() throws SQLException {
        waitDownload();

        List<WebElement> prices = getPrices(); // Цены
        List<WebElement> airlines = getAirlines(); // Авиакомпании
        List<WebElement> airportsDeparture = getDepartureAirport(); // Аэропорты отбытия
        List<WebElement> airportsArrival = getArrivalAirport(); // Аэропорты прибытия
        List<WebElement> timeArrival = getTimeArrival(); // Время прибытия
        List<WebElement> timeDeparture = getTimeDeparture(); // Время отбытия
        List<WebElement> datesArrival = getDateArrival(); // Дата прибытия
        List<WebElement> datesDeparture = getDateDeparture(); // Дата отбытия
        List<WebElement> timeTravel = getTravelTime(); // Время в пути

        List<CheapTickets> cheapTickets = new ArrayList<>(); // список ORM на таблицу билетов

        for (int i = 0; i < prices.size(); i++) {
            CheapTickets tickets = new CheapTickets();

            //Вынес дату и время для отправления и прибытия отдельно
            String dateDep = datesDeparture.get(i).getText();
            String timeDep = timeDeparture.get(i).getText();
            String dateArri = datesArrival.get(i).getText();
            String timeArri = timeArrival.get(i).getText();

            tickets.setAirlines(airlines.get(i).getAttribute("alt"));
            tickets.setArrivalAirport(airportsArrival.get(i).getAttribute("title"));
            tickets.setDepartureAirport(airportsDeparture.get(i).getAttribute("title"));
            tickets.setPriceTicket(prices.get(i).getText());
            tickets.setTravelTime(timeTravel.get(i).getText());
            tickets.setDepartureDateAndTime(dateDep + ", " + timeDep);
            tickets.setArrivalDateAndTime(dateArri + ", " + timeArri);

            cheapTickets.add(tickets);
        }

        Dao<CheapTickets, Void> dao =
                DaoManager.createDao(connection, CheapTickets.class); // ORMLite
        dao.create(cheapTickets); // Отправка данных в БД
    }

    @AfterClass
    public void tearDown() throws IOException {
        connection.close(); // Закрытие соединения
        driver.close(); // Закрытие браузера
    }

    @Step("Загрузка данных из файла конфига")
    private void loadProperties() throws IOException {
        FileInputStream fis = new FileInputStream("config.properties");
        Properties properties = new Properties();
        properties.load(fis);
        jdbcUrl = properties.getProperty("JDBC_URL");
        login = properties.getProperty("LOGIN");
        password = properties.getProperty("PASSWORD");
        parseUri = properties.getProperty("PARSE_URI");
        browser = hashBrowser.get(properties.getProperty("BROWSER"));
    }

    @Step("Заполнение hashMap всеми вариантами WebDriver")
    public void initHashMapBrowser() {
        hashBrowser.put("Chrome", new ChromeDriverInit());
        hashBrowser.put("FireFox", new FireFoxDriverInit());
        hashBrowser.put("IE", new IEDriverInit());
    }
}
