package modelPOM;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CheapTicketsPage {

    @FindBy(xpath = "/html/body/div[1]/div/div/div[2]/div/div[3]/div/div[2]/div/div[2]")
    public static WebElement container;

    @FindBy(xpath = "//div[@class='segment-route__time']")
    public static WebElement timeAndDateArrival;

    @FindBy(xpath = "//div[@class='segment-route__time']")
    public static WebElement timeAndDateDeparture;

    @FindBy(xpath = "//div[@class='flight-leg__flight-city']")
    public static WebElement nameAirportDepartureAndArrival;

    @FindBy(xpath = "//span[@class='price --rub']")
    public static WebElement priceTicket;

    @FindBy(xpath = "//div[@class='airline-logos__logo --square']")
    public static WebElement nameAirLine;

    @FindBy(xpath = "//div[@class='segment-route__path']")
    public static WebElement openInfoAirport;
}
