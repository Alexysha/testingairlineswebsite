package modelPOM;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

/**
 * @author Баскаков Алексей
 * Реализация действий с элементами с уже встроенным ожиданием отображения элемента
 * Уменьшает дубляж кода
 */
public class MyWebElementAction extends Page {
    private static WebDriver driver;

    private MyWebElementAction() {
    }

    /**
     * Передача драйвера фабрики PageFactory
     *
     * @param driver - веб-драйвер
     */
    public static void setDriver(WebDriver driver) {
        MyWebElementAction.driver = driver;
    }

    /**
     * Добаления страниц в фабрику PageFactory
     *
     * @param objects - массив page object
     */
    public static void setPageFactory(Object[] objects) {
        for (Object o : objects)
            PageFactory.initElements(driver, o);
    }

    /**
     * @return все элементы кнопок с ценами
     */
    public static List<WebElement> getPrices() {
        return driver.findElements(By.xpath("//span[@class='buy-button__price-num']"));
    }

    /**
     * @return элементы с временем прибытия
     */
    public static List<WebElement> getTimeArrival() {
        return driver.findElements(By.xpath("//div[@class='segment-route__destination']" +
                "//div[@class='segment-route__time']"));
    }

    /**
     * @return элемены с временем отправления
     */
    public static List<WebElement> getTimeDeparture() {
        return driver.findElements(By.xpath("//div[@class='segment-route__origin']" +
                "/div[@class='segment-route__time']"));
    }

    /**
     * @return элементы с датой прибытия
     */
    public static List<WebElement> getDateArrival() {
        return driver.findElements(By.xpath("//div[@class='segment-route__destination']" +
                "/div[@class='segment-route__date']"));
    }

    /**
     * @return элементы с датов отправления
     */
    public static List<WebElement> getDateDeparture() {
        return driver.findElements(By.xpath("//div[@class='segment-route__origin']" +
                "/div[@class='segment-route__date']"));
    }

    /**
     * @return элементы с продолжительностью переезда
     */
    public static List<WebElement> getTravelTime() {
        return driver.findElements(By.xpath("//div[@class='segment-route__total-time']"));
    }

    /**
     * @return элементы с аэропортами прибытия
     */
    public static List<WebElement> getArrivalAirport() {
        return driver.findElements(By.xpath("//div/div/div[1]/div[2]/div[2]/" +
                "div/div/div[2]/div[2]/div[5]/div[1]"));
    }

    /**
     * @return элементы с аэропортами отправления
     */
    public static List<WebElement> getDepartureAirport() {
        return driver.findElements(By.xpath("//div/div/div[1]/div[2]" +
                "/div[2]/div/div[1]/div[2]/div[2]/div[1]/div[1]"));
    }

    /**
     * @return элементы с информацией об авиакомпании
     */
    public static List<WebElement> getAirlines() {
        return driver.findElements(By.xpath("//div/div/div[1]/div[2]/div[1]/a/div/div[1]/img"));
    }

    public static void waitDownload() {
        new WebDriverWait(driver, 100).until(
                ExpectedConditions
                        .invisibilityOfElementLocated(
                                By.xpath("//div[@class='loader__stripes --animation-started']"))
        );
    }
}
