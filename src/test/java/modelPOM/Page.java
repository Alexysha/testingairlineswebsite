package modelPOM;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * @author Алексей Баскаков
 * Уменьшает дубляж кода при ожидании видимости элемента.
 * Суперкласс для POM.Page Object классов.
 */
abstract public class Page {
    /**
     * @param webElement - элемент разметки.
     * @param driver - экз. webdriver.
     * @param sec - макс. время ожидания появления элемента.
     */
    public static void WebDriverWaitPage(WebElement webElement, WebDriver driver, int sec) {
        new WebDriverWait(driver, sec)
                .until(ExpectedConditions
                        .visibilityOf(webElement));
    }
}
